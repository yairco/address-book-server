const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const cors = require('cors')
const app = express();
const port = process.env.PORT ?? 3030;
const db = new Map();
const metaData = require('./meta/response.json')
const TABLE_NAME='address'
const { randomUUID } = require('crypto');

app.use(cors());
app.use(
    express.urlencoded({
        extended: true
    })
)


const fillDB = async (db) =>{
    metaData.forEach((row,index)=>{
        db.set(`${TABLE_NAME}-${randomUUID()}`,JSON.stringify(row));
    })
    console.log('added new record to DB',db.values());
}

// Routes
app.get('/address',(req,res)=>{
    res.writeHead(200, { 'Content-Type': 'application/json'});

    let response=[];

    db.forEach(row=>{
        response.push(JSON.parse(row));
    })

    res.end(JSON.stringify({response: response}));
});

app.post('/address', (req,res)=>{
    // todo: sanitize and verify data
    db.set(`${TABLE_NAME}-${randomUUID()}`,JSON.stringify({
        date:req.body.date,
        name:req.body.name,
        address:req.body.address,
    }));

    let response=[];

    db.forEach(row=>{
        response.push(JSON.parse(row));
    })

    res.end(JSON.stringify({response: response}));
});


app.use((req, res,next)=>{
    res.status(404).send('Page not found');
});

fillDB(db).then(res=>{
    app.listen(port, () => {
        console.log(`This app is listening at http://localhost:${port}`)
    })
}).catch(e=>console.error(e));
