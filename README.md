# Address Book server

### Usage:
Create .env file in order to change default settings
```sh
npm install
```
#### Running application:
```sh
npm run start
```
###### works with [Address Book Client](https://bitbucket.org/yairco/address-book-client/src/master/)